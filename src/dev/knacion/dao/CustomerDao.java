package dev.knacion.dao;

import dev.knacion.model.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CustomerDao {

    private final String DB_BASE_URL = "jdbc:mariadb://localhost:3306/";
    private final String DB_NAME = "casestudydb";
    private final String USERNAME = "root";
    private final String PASSWORD = "root";

    private Connection connection;

    private void connect() throws SQLException {
        if (connection == null || connection.isClosed()) {
            try {
                Class.forName("org.mariadb.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                throw new SQLException(e);
            }
            connection = DriverManager.getConnection(
                    DB_BASE_URL + DB_NAME, USERNAME, PASSWORD);
        }
    }

    private void disconnect() throws SQLException {
        if (connection != null && !connection.isClosed()) {
            connection.close();
        }
    }

    public List<Customer> getAllCustomer() {
        List<Customer> customers = new ArrayList<>();

        try {
            connect();
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + DB_NAME + ".customer_table");

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                Customer customer = insertDataToObject(rs);
                customers.add(customer);
            }
            disconnect();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customers;
    }

    public Customer getCustomerById(UUID customerId) {
        Customer customer = null;

        try {
            connect();
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + DB_NAME + ".customer_table WHERE customer_id = ?");

            ps.setString(1, customerId.toString());
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                customer = insertDataToObject(rs);
            }
            disconnect();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customer;
    }

    public int insertCustomer(Customer customer) {
        int rowAffected = 0;
        try {
            connect();
            PreparedStatement ps = connection.prepareStatement("INSERT INTO " + DB_NAME + ".customer_table VALUES (?, ?, ?, ?, ?, ?)");
            ps.setString(1, customer.getCustomer_id().toString());
            ps.setString(2, customer.getFirstName());
            ps.setString(3, customer.getMiddleName());
            ps.setString(4, customer.getLastName());
            ps.setDate(5, customer.getBirthDate());
            ps.setDate(6, customer.getCardExpirationDate());

            rowAffected = ps.executeUpdate();
            disconnect();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rowAffected;
    }

    public int updateCustomer(Customer customer) {
        int i = 0;
        try {
            connect();
            PreparedStatement ps = connection.prepareStatement("UPDATE " + DB_NAME + ".customer_table " +
                    "SET first_name = ?" +
                    ", middle_name = ?" +
                    ", last_name = ?" +
                    ", birthdate = ?" +
                    ", card_expiration_date = ?" +
                    "WHERE customer_id = ?");

            ps.setString(1, customer.getFirstName());
            ps.setString(2, customer.getMiddleName());
            ps.setString(3, customer.getLastName());
            ps.setDate(4, customer.getBirthDate());
            ps.setDate(5, customer.getCardExpirationDate());
            ps.setString(6, customer.getCustomer_id().toString());

            i = ps.executeUpdate();
            disconnect();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;
    }

    private Customer insertDataToObject(ResultSet rs) throws SQLException {
        Customer customer = new Customer();
        customer.setCustomer_id(UUID.fromString(rs.getString("customer_id")));
        customer.setFirstName(rs.getString("first_name"));
        customer.setMiddleName(rs.getString("middle_name"));
        customer.setLastName(rs.getString("last_name"));
        customer.setBirthDate(rs.getDate("birthdate"));
        customer.setCardExpirationDate(rs.getDate("card_expiration_date"));
        return customer;
    }

    public List<Customer> searchCustomers(String query) {
        List<Customer> customerList = new ArrayList<>();

        try {
            connect();

            PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + DB_NAME + ".customer_table " +
                    "WHERE first_name LIKE ? " +
                    "OR middle_name like ? " +
                    "OR last_name like ?");

            ps.setString(1, "%" + query + "%");
            ps.setString(2, "%" + query + "%");
            ps.setString(3, "%" + query + "%");

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Customer customer = insertDataToObject(rs);
                customerList.add(customer);
            }

            disconnect();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customerList;
    }
}
