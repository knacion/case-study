package dev.knacion.model;

import java.sql.Date;
import java.util.UUID;

public class Customer {

    private UUID customer_id;
    private String firstName;
    private String middleName;
    private String lastName;
    private Date birthDate;
    private Date cardExpirationDate;

    public Customer() {
    }

    public Customer(UUID customer_id, String firstName, String middleName, String lastName, Date birthDate, Date cardExpirationDate) {
        this.customer_id = customer_id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.cardExpirationDate = cardExpirationDate;
    }

    public UUID getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(UUID customer_id) {
        this.customer_id = customer_id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getCardExpirationDate() {
        return cardExpirationDate;
    }

    public void setCardExpirationDate(Date cardExpirationDate) {
        this.cardExpirationDate = cardExpirationDate;
    }

}
