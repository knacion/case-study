package dev.knacion.servlet;

import dev.knacion.dao.CustomerDao;
import dev.knacion.dao.UserAccountDao;
import dev.knacion.model.Customer;
import dev.knacion.model.UserAccount;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(value = {"/home"})
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("login_username");
        String password = request.getParameter("login_password");

        UserAccountDao userDao = new UserAccountDao();
        UserAccount user = null;
        try {
            userDao.connect();
            user = userDao.getUser(username, password);
            userDao.disconnect();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (user != null) {
            CustomerDao dao = new CustomerDao();
            List<Customer> allCustomer = null;
            allCustomer = dao.getAllCustomer();
            request.setAttribute("memberList", allCustomer);
            request.getRequestDispatcher("/hompage_members.jsp").forward(request, response);
        } else {
            getServletContext().getRequestDispatcher("/error.jsp")
                    .forward(request, response);
        }
    }
}
