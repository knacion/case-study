package dev.knacion.servlet;

import dev.knacion.dao.CustomerDao;
import dev.knacion.model.Customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/search")
public class SearchServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String query = req.getParameter("q");

        CustomerDao dao = new CustomerDao();
        List<Customer> customers = dao.searchCustomers(query);

        req.setAttribute("memberList", customers);
        req.getRequestDispatcher("/hompage_members.jsp")
                .forward(req,resp);
    }
}
