<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/bulma.css">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LeWard | AdMember</title>
</head>
<body>


<div class="section">
    <div class="container">
        <h1 class="title">Add Member Page</h1>
        <form action="/members" method="post">
            <table>
                <tr>
                    <td><p class="subtitle">First name:</p></td>
                    <td><input class="input" type="text" required></td>
                </tr>
                <tr>
                    <td><p class="subtitle">Middle name:</p></td>
                    <td><input class="input" type="text"></td>
                </tr>
                <tr>
                    <td><p class="subtitle">Last name:</p></td>
                    <td><input type="text" class="input" required></td>
                </tr>
                <tr>
                    <td><p class="subtitle">Birthdate:</p></td>
                    <td><input class="input" type="date" required></td>
                </tr>
                <tr>
                    <td><p class="subtitle">Card Expiration Date:</p></td>
                    <td><input class="input" type="date" required></td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <button type="submit" class="button" name="add_member_btn">Cancel</button>
                        <button type="submit" class="button is-primary" name="add_member_btn">Add</button>
                    </td>
                </tr>


            </table>
        </form>

    </div>
</div>
</body>
</html>