<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bulma.css">

    <title>LeWards | Homepage</title>
</head>
<body>

<header>
    <nav class="navbar is-spaced has-shadow">
        <div class="container">
            <div class="navbar-brand">
                <a href="#" class="navbar-item is-outlined">
                    <p class="title is-uppercase">LeWards</p>
                    <a href="hompage_members.jsp" style="border-bottom: 2px" class="navbar-item is-active">Members</a>
                    <a href="point_transaction.jsp" class="navbar-item">Points Transaction</a>
                </a>
            </div>

            <div class="navbar-menu">
                <div class="navbar-start">

                </div>
                <div class="navbar-end">
                    <a class="navbar-item">
                        <strong>Sign Up</strong>
                    </a>
                    <div class="navbar-item ">
                        <div class="buttons">
                            <a href="#" class="button is-primary">
                                <strong>Log In</strong>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>


<div class="hero">
    <div class="hero-body">
        <div class="container">
            <form action="search" method="get" class="field is-horizontal">
                <a href="add_member.jsp" class="button is-primary is-outlined" style="margin: 8px;">Add Member</a>
                <input style="margin: 8px;" class="input" name="q" placeholder="Search members">
            </form>
        </div>
        <div class="container">
            <hr>

        </div>
        <div class="container">
            <table class="table is-hoverable is-fullwidth">
                <caption><p class="title">Member</p></caption>
                <thead>
                <tr>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Last Name</th>
                    <th>Birthdate</th>
                    <th style="text-align: center;">Action</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${memberList}" var="member">
                    <tr>
                        <td><c:out value="${member.getFirstName()}"/></td>
                        <td><c:out value="${member.getMiddleName()}"/></td>
                        <td><c:out value="${member.getLastName()}"/></td>
                        <td><c:out value="${member.getBirthDate()}"/></td>
                        <td style="text-align: center;"><a class="button" name="edit">Edit</a>
                            <a class="button is-danger" name="edit">Delete</a></td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
</html>