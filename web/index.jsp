<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LeWards | Login</title>
    <link rel="stylesheet" href="css/bulma.css">
</head>
<body>


<div class="container">
    <form action="home" method="POST" class="field">
        <caption><h1 class="title" align="center">LeWard</h1></caption>
        <table align="center" class="table is-fullwidth">
            <tr>
                <td>Username </td>
                <td><input class="input" type="text" name="login_username"></td>
            </tr>
            <tr>
                <td>Password </td>
                <td><input class="input" type="password" name="login_password"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                        <input type="submit" value="Login" class="button is-primary" name="login_button">
                    </div>
                </td>
            </tr>
        </table>
    </form>
</div>


</body>
</html>